package pl.com.costs_plus.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.com.costs_plus.Exceptions.MonthlyNotFoundException;
import pl.com.costs_plus.model.Bundle;
import pl.com.costs_plus.model.Game;
import pl.com.costs_plus.model.User;
import pl.com.costs_plus.model.dao.SortAndUserDAO;
import pl.com.costs_plus.repository.BundleRepository;
import pl.com.costs_plus.repository.UserRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BundleService {

    @Autowired
    private BundleRepository bundleRepository;

    @Autowired
    private UserRepository userRepository;

    public List<Bundle> findAll() {
        return bundleRepository.findAll();
    }

    public Bundle getBundleById(int id) {
        Optional<Bundle> optionalHumbleMonthly = bundleRepository.findById(id);

        if (!optionalHumbleMonthly.isPresent())
            throw new MonthlyNotFoundException("Bundle with id: " + id + " doesn't exist ");

        return optionalHumbleMonthly.get();
    }

    public ResponseEntity<Bundle> addBundle(Bundle bundle) {
        bundleRepository.save(bundle);
        Bundle newBundle = bundle;
        return new ResponseEntity<Bundle>(newBundle, HttpStatus.OK);
    }

    public ResponseEntity<Bundle> modifyBundle(int id, Bundle bundle) throws Exception {

        Optional<Bundle> currentOptionalBundle = bundleRepository.findById(id);

        if (!currentOptionalBundle.isPresent())
            throw new Exception("Bundle with id: " + id + " doesn't exist ");

        Bundle currentBundle = currentOptionalBundle.get();

        if (bundle.getDateBuy() != null) {
            currentBundle.setDateBuy(bundle.getDateBuy().plusDays(1));
        }
        if (bundle.getName() != null) {
            currentBundle.setName(bundle.getName());
        }
        if (bundle.getCostUSD() != null) {
            currentBundle.setCostUSD(bundle.getCostUSD());
        }
        if (bundle.getCostPLN() != null) {
            currentBundle.setCostPLN(bundle.getCostPLN());
        }

        bundleRepository.save(currentBundle);
        return new ResponseEntity<Bundle>(currentBundle, HttpStatus.OK);
    }


    public void deleteById(int id) {
        bundleRepository.deleteById(id);
    }


    public List<Bundle> getSort(int id) {

        List<Bundle> bundleList = bundleRepository.findAll();

        sortList(id, bundleList);

        return bundleList;
    }

    private void sortList(int id, List<Bundle> bundleList) {
        calculateSold(bundleList);

        switch (id) {

            case (1):
                Collections.sort(bundleList, new Comparator<Bundle>() {
                    @Override
                    public int compare(Bundle o1, Bundle o2) {
                        return o1.getName().compareTo(o2.getName());
                    }
                });
                break;

            case (2):
                Collections.sort(bundleList, new Comparator<Bundle>() {
                    @Override
                    public int compare(Bundle o1, Bundle o2) {
                        return o1.getDateBuy().compareTo(o2.getDateBuy());
                    }
                });
                break;

            case (3):
                Collections.sort(bundleList, new Comparator<Bundle>() {
                    @Override
                    public int compare(Bundle o1, Bundle o2) {
                        if (o1.getCostUSD() == null) {
                            o1.setCostUSD(0.0);
                        }
                        if (o2.getCostUSD() == null) {
                            o2.setCostUSD(0.0);
                        }
                        if (o1.getCostUSD() > o2.getCostUSD()) {
                            return -1;
                        } else if (o1.getCostUSD() < o2.getCostUSD()) {
                            return 1;
                        }
                        return 0;
                    }
                });
                break;

            case (4):
                Collections.sort(bundleList, new Comparator<Bundle>() {
                    @Override
                    public int compare(Bundle o1, Bundle o2) {
                        if (o1.getCostPLN() == null) {
                            o1.setCostPLN(0.0);
                        }
                        if (o2.getCostPLN() == null) {
                            o2.setCostPLN(0.0);
                        }
                        if (o1.getCostPLN() > o2.getCostPLN()) {
                            return -1;
                        } else if (o1.getCostPLN() < o2.getCostPLN()) {
                            return 1;
                        }
                        return 0;
                    }
                });
                break;

            case (5):
                Collections.sort(bundleList, new Comparator<Bundle>() {
                    @Override
                    public int compare(Bundle o1, Bundle o2) {

                        if (o1.getSold() > o2.getSold()) {
                            return -1;
                        } else if (o1.getSold() < o2.getSold()) {
                            return 1;
                        }
                        return 0;
                    }
                });
                break;

        }
    }

    private void calculateSold(List<Bundle> bundleList) {
        for (Bundle bundle : bundleList) {
            double sum = 0;
            for (Game game : bundle.getGameList()) {
                if (game.getSalePrcie() != null)
                    sum += game.getSalePrcie();
                bundle.setSold(sum);
            }
        }
    }

    public List<Game> getAllGamesById(int id) {
        Optional<Bundle> optionalHumbleMonthly = bundleRepository.findById(id);

        if (!optionalHumbleMonthly.isPresent())
            throw new MonthlyNotFoundException("Bundle with id: " + id + " doesn't exist ");

        return optionalHumbleMonthly.get().getGameList();
    }


    public ResponseEntity<Bundle> addNewBundleToUser(Bundle bundle, String username) {
        bundle.setDateBuy(bundle.getDateBuy().plusDays(1));
        Bundle newBundle = bundle;
        newBundle.setUser(userRepository.findByUsername(username).get());
        bundleRepository.save(newBundle);
        return new ResponseEntity<Bundle>(newBundle, HttpStatus.OK);
    }

    public List<Bundle> getByUser(String username) {
        User user = userRepository.findByUsername(username).get();

        List<Bundle> bundleList = bundleRepository.findAll();

        List<Bundle> filterBundleList = bundleList.stream()
                .filter(bundle -> bundle.getUser() == user )
                .collect(Collectors.toList());

        return filterBundleList;
    }


    public List<Bundle> getSortAndUser(SortAndUserDAO sortAndUserDAO) {
        List<Bundle> bundleList = getByUser(sortAndUserDAO.getUsername());
        sortAndUser(Integer.parseInt(sortAndUserDAO.getSortId()), bundleList);
        return bundleList;
    }

    private void sortAndUser(int id, List<Bundle> bundleList) {
        sortList(id, bundleList);
    }

}
