package pl.com.costs_plus.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;


@Entity
public class Bundle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "datebuy")
    private LocalDate dateBuy;

    @Column(name = "nazwa")
    private String name;
    private Double costUSD;
    private Double costPLN;

    @OneToMany(mappedBy = "bundle", cascade = CascadeType.ALL)
    private List<Game> gameList;

    @JsonIgnore
    @ManyToOne
    private User user;

    public Bundle() {
    }

    public Bundle(LocalDate dateBuy, String name, Double costUSD, Double costPLN, List<Game> gameList, User user, double sold) {
        this.dateBuy = dateBuy;
        this.name = name;
        this.costUSD = costUSD;
        this.costPLN = costPLN;
        this.gameList = gameList;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDateBuy() {
        return dateBuy;
    }

    public void setDateBuy(LocalDate dateBuy) {
        this.dateBuy = dateBuy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCostUSD() {
        return costUSD;
    }

    public void setCostUSD(Double costUSD) {
        this.costUSD = costUSD;
    }

    public Double getCostPLN() {
        return costPLN;
    }

    public void setCostPLN(Double costPLN) {
        this.costPLN = costPLN;
    }

    public List<Game> getGameList() {
        return gameList;
    }

    public void setGameList(List<Game> gameList) {
        this.gameList = gameList;
    }

    public double getSold() {
        return sold;
    }

    public void setSold(double sold) {
        this.sold = sold;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Bundle{" +
                "id=" + id +
                ", dateBuy=" + dateBuy +
                ", name='" + name + '\'' +
                ", costUSD=" + costUSD +
                ", costPLN=" + costPLN +
                ", gameList=" + gameList +
                ", user=" + user +
                ", sold=" + sold +
                '}';
    }

    private double sold;
}
