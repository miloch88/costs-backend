package pl.com.costs_plus.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private Double salePrcie;

    @JsonIgnore
    @ManyToOne
    private Bundle bundle;

    public Game() {
    }

    public Game(String name, Double salePrcie, Bundle bundle) {
        this.name = name;
        this.salePrcie = salePrcie;
        this.bundle = bundle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSalePrcie() {
        return salePrcie;
    }

    public void setSalePrcie(Double salePrcie) {
        this.salePrcie = salePrcie;
    }

    public Bundle getBundle() {
        return bundle;
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }
}
