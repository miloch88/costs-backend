package pl.com.costs_plus.model.dao;

public class SortAndUserDAO {

    private String sortId;
    private String username;

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
