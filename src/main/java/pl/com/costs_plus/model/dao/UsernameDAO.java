package pl.com.costs_plus.model.dao;

public class UsernameDAO {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
