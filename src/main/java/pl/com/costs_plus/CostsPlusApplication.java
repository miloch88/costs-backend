package pl.com.costs_plus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CostsPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(CostsPlusApplication.class, args);
    }

}

