package pl.com.costs_plus.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MonthlyNotFoundException extends RuntimeException {
    public MonthlyNotFoundException(String s) {
        super(s);
    }
}
