package pl.com.costs_plus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.costs_plus.model.Game;

@Repository
public interface GameRepository extends JpaRepository<Game, Integer> {
}
