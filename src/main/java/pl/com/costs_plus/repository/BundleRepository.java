package pl.com.costs_plus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.com.costs_plus.model.Bundle;

import java.util.List;

@Repository
public interface BundleRepository extends JpaRepository<Bundle, Integer> {
    List<Bundle> findByUserId(Long i);
}
