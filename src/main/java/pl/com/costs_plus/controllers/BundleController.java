package pl.com.costs_plus.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.com.costs_plus.Exceptions.MonthlyNotFoundException;
import pl.com.costs_plus.model.Bundle;
import pl.com.costs_plus.model.Game;
import pl.com.costs_plus.model.dao.SortAndUserDAO;
import pl.com.costs_plus.model.dao.UsernameDAO;
import pl.com.costs_plus.repository.GameRepository;
import pl.com.costs_plus.repository.BundleRepository;
import pl.com.costs_plus.services.BundleService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/bundle")
public class BundleController {

    @Autowired
    private BundleRepository bundleRepository;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    BundleService bundleService;

    @GetMapping("")
    public List<Bundle> getAllBundles() {
        return bundleService.getSort(2);
    }

    @GetMapping("sort")
    public List<Bundle> getSortByDate(@RequestParam("sortId") int id) {
        return bundleService.getSort(id);
    }

    @GetMapping("/{id}")
    public Bundle getMonthlyById(@PathVariable int id) {
        return bundleService.getBundleById(id);
    }

    @PostMapping("")
    public ResponseEntity<Bundle> addBundle(@Valid @RequestBody Bundle bundle) {
        return bundleService.addBundle(bundle);
    }

    @PostMapping("add")
    public ResponseEntity<Bundle> addBundleDao(@RequestParam ("user") String username, @Valid @RequestBody Bundle bundle) {
        return bundleService.addNewBundleToUser(bundle, username);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Bundle> modifyBundle(@PathVariable int id, @RequestBody Bundle bundle) throws Exception {
        return bundleService.modifyBundle(id, bundle);
    }

    @DeleteMapping("/{id}")
    public void deleteByMonthly(@PathVariable int id) {
        bundleService.deleteById(id);
    }

    @GetMapping("/{id}/games")
    public List<Game> getAllGamesByMonthly(@PathVariable int id) {
       return bundleService.getAllGamesById(id);
    }

    @PostMapping("/games")
    public ResponseEntity createGame(@RequestBody Game game) {


        Optional<Bundle> optionalHumbleMonthly = bundleRepository.findById(game.getId());

        if (!optionalHumbleMonthly.isPresent())
            throw new MonthlyNotFoundException("Bundle with id: " + game.getId() + " doesn't exist ");

        Bundle monthly = optionalHumbleMonthly.get();

        game.setBundle(monthly);
        gameRepository.save(game);

        return new ResponseEntity<Game>(game, HttpStatus.OK);
    }

    @GetMapping("/games/{id}")
    public Game getGame(@PathVariable int id) {
        Optional<Game> optionalGame = gameRepository.findById(id);

        if (!optionalGame.isPresent())
            throw new MonthlyNotFoundException("Bundle with id: " + id + " doesn't exist ");

        return optionalGame.get();
    }

    @PutMapping("/games/{id}")
    public ResponseEntity<Game> modifyGame(@PathVariable int id, @RequestBody Game game) {
        Optional<Game> optionalGame = gameRepository.findById(id);

        Game currentGame = optionalGame.get();

        if (game.getName() != null) {
            currentGame.setName(game.getName());
        }

        if (game.getSalePrcie() != null) {
            currentGame.setSalePrcie(game.getSalePrcie());
        }

        gameRepository.save(currentGame);

        return new ResponseEntity<Game>(currentGame, HttpStatus.OK);
    }

    @DeleteMapping("/games/{id}")
    public void deleteGameById(@PathVariable int id) {
        gameRepository.deleteById(id);
    }

    @GetMapping("getBundleId/{id}")
    public Integer getBudleIdByGame(@PathVariable int id) {
        Optional<Game> optionalGame = gameRepository.findById(id);
        Game game = optionalGame.get();

        return game.getBundle().getId();
    }

    @PostMapping("/byuser")
    public List<Bundle> getByUser(@RequestBody UsernameDAO usernameDAO){
        return bundleService.getByUser(usernameDAO.getUsername());
    }

    @PostMapping("/sortanduser")
    public List<Bundle> getSortAndUser(@RequestBody SortAndUserDAO sortAndUserDAO){
        return bundleService.getSortAndUser(sortAndUserDAO);
    }
}

